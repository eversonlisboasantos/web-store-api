package com.webstore.repository.impl;

import com.webstore.mapper.MenuMapper;
import com.webstore.model.Menu;
import com.webstore.querys.QueryMenu;
import com.webstore.repository.MenuRepository;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuRepositoryImpl extends BaseRepository implements MenuRepository {

    @Override
    public List<Menu> findMenusByPerfil(Long codigo) {
        return getNamedParameterJdbcTemplate().query(QueryMenu.queryMenusByPerfil(), new MapSqlParameterSource()
            .addValue("perfil", codigo), new MenuMapper());
    }
}
