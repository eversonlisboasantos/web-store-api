package com.webstore.repository.impl;

import com.webstore.mapper.UsuarioMapper;
import com.webstore.model.Usuario;
import com.webstore.querys.QueryUsuario;
import com.webstore.repository.UserRepository;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class UserRepositoryImpl extends BaseRepository implements UserRepository{

    @Override
    public Optional<Usuario> findUsuarioByLogin(String login) {
        return Optional.of(getNamedParameterJdbcTemplate().queryForObject(QueryUsuario.queryUsuarioByLogin(), new MapSqlParameterSource()
                .addValue("login", login), new UsuarioMapper()));
    }

    @Override
    public Optional<Usuario> findUsuarioByCodigo(Long codigo) {
        return Optional.of(getNamedParameterJdbcTemplate().queryForObject(QueryUsuario.queryUsuarioByCodigo(), new MapSqlParameterSource()
                .addValue("codigo", codigo), new UsuarioMapper()));
    }
}
