package com.webstore.repository;

import com.webstore.model.Menu;

import java.util.List;

public interface MenuRepository {
    public List<Menu> findMenusByPerfil(Long codigo);
}
