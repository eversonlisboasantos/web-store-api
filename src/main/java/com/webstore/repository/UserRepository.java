package com.webstore.repository;

import com.webstore.model.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    Optional<Usuario> findUsuarioByLogin(String login);
    Optional<Usuario> findUsuarioByCodigo(Long codigo);
}
