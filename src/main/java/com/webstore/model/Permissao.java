package com.webstore.model;

import java.util.ArrayList;
import java.util.List;

public class Permissao extends AbstractEntity {

    private List<Menu> menus;

    public Permissao() {
        this.menus = new ArrayList<>();
    }

    public List<Menu> getMenus() {
        return menus;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }
}
