package com.webstore.model;

public class Perfil extends AbstractEntity {

    private Long codigoPerfil;

    private String perfil;

    private String descricao;

    private String situacao;

    private Permissao permissao;;

    public Perfil() {
        this.permissao = new Permissao();
    }

    public Long getCodigoPerfil() {
        return codigoPerfil;
    }

    public void setCodigoPerfil(Long codigoPerfil) {
        this.codigoPerfil = codigoPerfil;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public Permissao getPermissao() {
        return permissao;
    }

    public void setPermissao(Permissao permissao) {
        this.permissao = permissao;
    }
}
