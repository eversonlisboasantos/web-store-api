package com.webstore.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;


public class Usuario  extends AbstractEntity {

    private Long codigoUsuario;

    private String login;

    private String senha;

    private String primeiroAcesso;

    private Long usuarioCadastro;

    private Long codigoPerfil;

    private Membro membro;

    private Perfil perfil;

    public Usuario() {
        this.perfil = new Perfil();
        this.membro = new Membro();
    }

    public Long getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(Long codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getPrimeiroAcesso() {
        return primeiroAcesso;
    }

    public void setPrimeiroAcesso(String primeiroAcesso) {
        this.primeiroAcesso = primeiroAcesso;
    }

    public Long getUsuarioCadastro() {
        return usuarioCadastro;
    }

    public void setUsuarioCadastro(Long usuarioCadastro) {
        this.usuarioCadastro = usuarioCadastro;
    }

    public Long getCodigoPerfil() {
        return codigoPerfil;
    }

    public void setCodigoPerfil(Long codigoPerfil) {
        this.codigoPerfil = codigoPerfil;
    }

    public Membro getMembro() {
        return membro;
    }

    public void setMembro(Membro membro) {
        this.membro = membro;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }


}
