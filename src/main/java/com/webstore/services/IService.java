package com.webstore.services;

import java.util.List;

public interface IService <T, Long> {


    T save (T t);
    T findByCodigo(Long codigo);
    List<T> findAll();
    void delete (Long codigo);
}
