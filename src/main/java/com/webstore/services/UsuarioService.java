package com.webstore.services;

import com.webstore.model.Menu;
import com.webstore.model.Perfil;
import com.webstore.model.Usuario;
import com.webstore.repository.impl.MenuRepositoryImpl;
import com.webstore.repository.impl.UserRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService implements IService<Usuario, Long> {

    @Autowired
    private UserRepositoryImpl userRepository;

    @Autowired
    private MenuRepositoryImpl menuRepository;

    @Override
    public Usuario save(Usuario usuario) {
        return null;
    }

    @Override
    public Usuario findByCodigo(Long codigo) {
        Optional<Usuario> usuario = this.userRepository.findUsuarioByCodigo(codigo);
        Optional<Perfil> perfil = Optional.of(usuario.get().getPerfil());
        Optional.ofNullable(perfil).ifPresent(p -> {
            usuario.get().getPerfil().getPermissao().setMenus(menuRepository.findMenusByPerfil(p.get().getCodigoPerfil()));
        });

        return usuario.get();
    }

    @Override
    public List<Usuario> findAll() {
        return null;
    }

    @Override
    public void delete(Long codigo) {

    }

    public Usuario findUsuarioByLogin(String login) {
        return this.userRepository.findUsuarioByLogin(login).get();
    }
}
