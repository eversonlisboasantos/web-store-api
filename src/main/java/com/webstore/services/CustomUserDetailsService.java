package com.webstore.services;

import com.webstore.model.Usuario;
import com.webstore.repository.impl.UserRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Component
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepositoryImpl usuarioRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {

        UsernameNotFoundException exception = new UsernameNotFoundException("");

        List<GrantedAuthority> authorityListAdmin = AuthorityUtils.createAuthorityList("ROLE_USER", "ROLE_ADMIN");
        List<GrantedAuthority> authorityListDev = AuthorityUtils.createAuthorityList("ROLE_USER");
        Optional<Usuario> usuario = usuarioRepository.findUsuarioByLogin(login);

        if (!usuario.isPresent())
            throw new UsernameNotFoundException("Usuario não encontrado");

        return new User(usuario.get().getLogin(), usuario.get().getSenha(), authorityListAdmin);
    }
}
