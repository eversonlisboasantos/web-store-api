package com.webstore.dominios;

public enum DominioSimNao {

    ATIVO("Sim", "S"), EXCLUIDO("Não", "N");

    private String descricao;
    private String sigla;

    DominioSimNao(String descricao, String sigla) {
        this.descricao = descricao;
        this.sigla = sigla;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    static public String getSimNao(String sigla){
        for (DominioSimNao simNao: DominioSimNao.values()) {
            if(sigla.equalsIgnoreCase(simNao.getSigla()))
                return simNao.descricao;
        }
        return "";
    }
}
