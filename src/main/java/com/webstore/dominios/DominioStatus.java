package com.webstore.dominios;

public enum DominioStatus {

    ATIVO("Ativo", "A"), EXCLUIDO("Excluído", "E");

    private String descricao;
    private String sigla;

    DominioStatus(String descricao, String sigla) {
        this.descricao = descricao;
        this.sigla = sigla;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    static public String getStatus(String sigla){
        for (DominioStatus status: DominioStatus.values()) {
            if(sigla.equalsIgnoreCase(status.getSigla()))
                return status.descricao;
        }
        return "";
    }
}
