package com.webstore.controller;

import com.webstore.exception.BusinessException;
import com.webstore.repository.UserRepository;
import com.webstore.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("v1")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping(path = "public/usuario/findByLogin/{login}")
    public ResponseEntity<?> findUsuarioByLogin(@PathVariable String login) throws BusinessException {
        return new ResponseEntity<>(this.usuarioService.findUsuarioByLogin(login), HttpStatus.OK);
    }

    @GetMapping(path = "public/usuario/findByCodigo/{codigo}")
    public ResponseEntity<?> findUsuarioByCodigo(@PathVariable Long codigo) throws BusinessException {
        return new ResponseEntity<>(this.usuarioService.findByCodigo(codigo), HttpStatus.OK);
}



}
