package com.webstore.controller;

import com.webstore.bean.ResponseErrorBean;
import com.webstore.exception.BusinessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {


    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<ResponseErrorBean> handleBusinessException(BusinessException e){
        ResponseErrorBean response = e.getResponse();
        response.setTimeStamp(LocalDateTime.now());
        return  new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    public ResponseEntity<ResponseErrorBean> handleMethodArgumentTypeMismatchException (MethodArgumentTypeMismatchException e){

        List<String> messages = new ArrayList<>();
        messages.add(e.getName() + " deve ser do tipo "+ e.getRequiredType().getSimpleName());
        ResponseErrorBean response = new ResponseErrorBean(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase(), e.getMessage(), messages);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({EntityNotFoundException.class})
    public ResponseEntity<ResponseErrorBean> handleEntityNotFoundException (EntityNotFoundException e){
        List<String> messages = new ArrayList<>();
        messages.add(e.getMessage());
        ResponseErrorBean response = new ResponseErrorBean(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND.getReasonPhrase(), e.getMessage(), messages);
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<ResponseErrorBean> handleException (Exception e){
        ResponseErrorBean response = new ResponseErrorBean(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), e.getMessage(), null);
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
