package com.webstore.mapper;

import com.webstore.dominios.DominioSimNao;
import com.webstore.dominios.DominioStatus;
import com.webstore.model.Usuario;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;


public class UsuarioMapper implements RowMapper<Usuario> {

    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    @Override
    public Usuario mapRow(ResultSet resultSet, int i) throws SQLException {
        Usuario usuario = new Usuario();
        usuario.setCodigoUsuario(resultSet.getLong("cod_usuario"));
        usuario.setLogin(resultSet.getString("usr_login"));
        usuario.setPrimeiroAcesso(DominioSimNao.getSimNao(resultSet.getString("idt_pri_acesso")));
        usuario.setDataCadastro(sdf.format(resultSet.getTimestamp("usr_dat_cadastro")));
        usuario.setDataAtualizacao(sdf.format(resultSet.getTimestamp("usr_dat_atualizacao")));
        usuario.getPerfil().setCodigoPerfil(resultSet.getLong("cod_usr_perfil"));
        usuario.getPerfil().setPerfil(resultSet.getString("nme_perfil"));
        usuario.getPerfil().setDescricao(resultSet.getString("dsc_perfil"));
        usuario.getPerfil().setSituacao(DominioStatus.getStatus(resultSet.getString("idt_sit_perfil")));
        usuario.getPerfil().setDataCadastro(sdf.format(resultSet.getTimestamp("prf_dat_cadastro")));
        usuario.getPerfil().setDataAtualizacao(sdf.format(resultSet.getTimestamp("prf_dat_atualizacao")));
        return usuario;
    }

}
