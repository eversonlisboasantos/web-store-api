package com.webstore.mapper;

import com.webstore.dominios.DominioStatus;
import com.webstore.model.Menu;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MenuMapper implements RowMapper<Menu> {

    @Override
    public Menu mapRow(ResultSet resultSet, int i) throws SQLException {
        Menu menu = new Menu();
        menu.setMenu(resultSet.getString("nme_menu"));
        menu.setUlr(resultSet.getString("dsc_url_menu"));
        return menu;
    }
}
