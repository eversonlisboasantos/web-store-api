package com.webstore.bean;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ResponseErrorBean implements Serializable {

    private int httpStatusCode;
    private String httpStatusMessage;
    private String detalhes;
    private List<String> messages;
    private LocalDateTime timeStamp;


    public ResponseErrorBean() {
        this.httpStatusCode = Integer.valueOf("0");
        this.httpStatusMessage = "";
        this.detalhes = "";
        this.messages = new ArrayList<>();
        this.timeStamp = LocalDateTime.now();
    }

    public ResponseErrorBean(int httpStatusCode, String httpStatusMessage, String detalhes, List<String> messages) {
        this.httpStatusCode = httpStatusCode;
        this.httpStatusMessage = httpStatusMessage;
        this.detalhes = detalhes;
        this.messages = messages;
        this.timeStamp = LocalDateTime.now();
    }

    public int getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(int httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    public String getHttpStatusMessage() {
        return httpStatusMessage;
    }

    public void setHttpStatusMessage(String httpStatusMessage) {
        this.httpStatusMessage = httpStatusMessage;
    }

    public String getDetalhes() {
        return detalhes;
    }

    public void setDetalhes(String detalhes) {
        this.detalhes = detalhes;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }
}
