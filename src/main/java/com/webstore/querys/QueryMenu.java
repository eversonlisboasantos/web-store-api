package com.webstore.querys;

public class QueryMenu {

    public static String queryMenusByPerfil (){
        StringBuilder query = new StringBuilder();
        query.append("SELECT m.nme_menu, m.dsc_url_menu");
        query.append(" FROM permissoes p INNER JOIN menus m ON ");
        query.append(" m.cod_menu = p.cod_menu AND m.idt_sit_menu = 'A' ");
        query.append(" WHERE p.cod_perfil = :perfil");
        return query.toString();
    }
}
