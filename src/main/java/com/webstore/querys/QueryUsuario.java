package com.webstore.querys;

public class  QueryUsuario {


    public static String queryUsuarioByLogin(){
        String where = "WHERE u.usr_login = :login ";
        return queryUsuario().append(where).toString();
    }

    public static String queryUsuarioByCodigo(){
        String where = "WHERE u.cod_usuario = :codigo ";
        return queryUsuario().append(where).toString();
    }

    public static StringBuilder queryUsuario(){
        StringBuilder query = new StringBuilder();
        query.append(" SELECT u.cod_usuario,");
        query.append(" u.usr_login,");
        query.append(" u.usr_senha,");
        query.append(" u.idt_pri_acesso,");
        query.append(" u.usr_dat_cadastro,");
        query.append(" u.usr_dat_atualizacao,");
        query.append(" u.cod_usr_perfil,");
        query.append(" p.nme_perfil,");
        query.append(" p.dsc_perfil,");
        query.append(" p.idt_sit_perfil,");
        query.append(" p.prf_dat_cadastro,");
        query.append(" p.prf_dat_atualizacao,");
        query.append(" u.cod_membro");
        query.append(" FROM usuarios u ");
        query.append(" INNER JOIN perfis p ON p.cod_perfil = u.cod_usr_perfil ");
        return query;
    }
}
