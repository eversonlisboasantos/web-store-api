package com.webstore.exception;

import com.webstore.bean.ResponseErrorBean;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.LocalDateTime;
import java.util.List;

@ResponseStatus
public class BusinessException extends RuntimeException {

    private ResponseErrorBean response;

    public BusinessException() {
        super();
    }

    public BusinessException(List<String> messages) {
        super();
        response = new ResponseErrorBean();
        response.setHttpStatusCode(HttpStatus.BAD_REQUEST.value());
        response.setHttpStatusMessage(HttpStatus.BAD_REQUEST.getReasonPhrase());
        response.setMessages(messages);
        response.setTimeStamp(LocalDateTime.now());
    }

    public ResponseErrorBean getResponse() {
        return response;
    }
}
